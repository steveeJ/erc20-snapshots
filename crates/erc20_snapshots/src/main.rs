use human_repr::HumanDuration;
use web3::types::BlockNumber;

mod storage {
    pub const DB_KEY_CONTINUE_AT_BLOCK: &str = "continue_at_block";
}

#[tokio::main(flavor = "multi_thread")]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let transport = web3::transports::Http::new("https://eth-rpc.gateway.pokt.network")?;
    let web3 = web3::Web3::new(transport);

    let latest_block_number = web3
        .eth()
        .block(web3::types::BlockId::Number(BlockNumber::Latest))
        .await?
        .unwrap()
        .number
        .unwrap();

    // store the fetched blocks here
    let db = sled::open("eth_blocks")?;

    let continue_at_block = if let Some(contine_at_block) = db
        .get(crate::storage::DB_KEY_CONTINUE_AT_BLOCK)?
        .map(|v| web3::types::U64::from_big_endian(&v))
    {
        contine_at_block
    } else {
        web3.eth()
            .block(web3::types::BlockId::Number(BlockNumber::Earliest))
            .await?
            .unwrap()
            .number
            .unwrap()
    };

    println!(
        "fetching blocks {}..{:?}",
        continue_at_block, latest_block_number
    );

    use std::io::Write;
    let mut stdout = std::io::stdout();

    let start = std::time::Instant::now();
    let start_block = continue_at_block.as_u64();
    let end_block = latest_block_number.as_u64();

    // TODO: fetch concurrently
    for block_number in start_block..end_block {
        print!("fetching block {}...", block_number);
        stdout.flush()?;

        let block = web3
            .eth()
            .block(web3::types::BlockId::Number(block_number.into()))
            .await?
            .unwrap();

        let block_bytes = bincode::serialize(&block)?;
        db.insert(block_number.to_be_bytes(), block_bytes)?;

        db.insert(
            crate::storage::DB_KEY_CONTINUE_AT_BLOCK,
            &(block_number + 1).to_be_bytes(),
        )?;

        let duration = std::time::Instant::now() - start;
        let avg_speed = ((block_number - start_block) as f64) / duration.as_secs_f64();
        let time_remaining = if avg_speed > 0.0 {
            std::time::Duration::from_secs_f64((end_block - block_number) as f64 / avg_speed)
        } else {
            Default::default()
        };

        println!(
            "done! {:.1} blocks/s. time remaining: {}",
            avg_speed,
            time_remaining.human_duration()
        );
    }

    Ok(())
}
